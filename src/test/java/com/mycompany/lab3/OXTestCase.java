/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Gxz32
 */
public class OXTestCase {

    public OXTestCase() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinZeroTurnBy_X_False() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(false, OXTest.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRoW1By_X_True() {
        String[][] table = {{"X", "X", "X"}, {"O", "-", "-"}, {"-", "O", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRoW2By_X_True() {
        String[][] table = {{"-", "O", "-"}, {"X", "X", "X"}, {"-", "-", "O"}};
        String currentPlayer = "X";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRoW3By_X_True() {
        String[][] table = {{"-", "O", "-"}, {"O", "-", "-"}, {"X", "X", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinZeroTurnBy_O_False() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(false, OXTest.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRoW1By_O_True() {
        String[][] table = {{"O", "O", "O"}, {"X", "-", "-"}, {"-", "X", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRoW2By_O_True() {
        String[][] table = {{"-", "X", "-"}, {"O", "O", "O"}, {"-", "-", "X"}};
        String currentPlayer = "O";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRoW3By_O_True() {
        String[][] table = {{"-", "X", "-"}, {"X", "-", "-"}, {"O", "O", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCoL1By_X_True() {
        String[][] table = {{"X", "O", "-"}, {"X", "-", "-"}, {"X", "O", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCoL2By_X_True() {
        String[][] table = {{"-", "X", "O"}, {"O", "X", "-"}, {"-", "X", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCoL3By_X_True() {
        String[][] table = {{"-", "O", "X"}, {"O", "-", "X"}, {"-", "O", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCoL1By_O_True() {
        String[][] table = {{"O", "X", "-"}, {"O", "-", "-"}, {"O", "X", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCoL2By_O_True() {
        String[][] table = {{"-", "O", "X"}, {"X", "O", "-"}, {"-", "O", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCoL3By_O_True() {
        String[][] table = {{"-", "X", "O"}, {"X", "-", "O"}, {"-", "X", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckDiagonals1By_X_True() {
        String[][] table = {{"X", "-", "O"}, {"-", "X", "O"}, {"-", "O", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckDiagonals2By_X_True() {
        String[][] table = {{"-", "O", "X"}, {"-", "X", "O"}, {"X", "O", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckDiagonals1By_O_True() {
        String[][] table = {{"O", "-", "X"}, {"-", "O", "X"}, {"-", "X", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckDiagonals2By_O_True() {
        String[][] table = {{"-", "X", "O"}, {"-", "O", "X"}, {"O", "X", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXTest.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testIsTie(){    
        String[][] table = {{"X", "O", "X"}, {"X", "O", "O"}, {"O", "X", "X"}};
        String currentPlayer = "X";
        assertEquals(false, OXTest.checkWin(table, currentPlayer));
    }
}
